<?php
require __DIR__ . '/../vendor/autoload.php';

use mef\TokenGenerator\TokenGenerator;
use mef\TokenGenerator\UuidGenerator;
use mef\TokenGenerator\CallbackTokenGenerator;

$t = new TokenGenerator(10, TokenGenerator::ALPHANUMERIC_CHARSET);

echo "Using default token size of " . $t->getLength(), "\n";
echo "Using default charset of " . $t->getCharset(), "\n";

echo "Generate one token:\n";
echo $t->generate(), "\n\n";

echo "Generate five more:\n";
foreach ($t->iterate(5) as $token)
{
	echo $token, "\n";
}
echo "\n";

echo "Generate ten more:\n";
foreach (new LimitIterator($t->getIterator(), 0, 10) as $token)
{
	echo $token, "\n";
}
echo "\n";

echo "Random UUID:\n";
echo (new UuidGenerator(UuidGenerator::LOWERCASE_FORMAT))->generate(), "\n\n";

$rawUuidGenerator = new UuidGenerator(UuidGenerator::RAW_FORMAT);
$base64UuidGenerator = new CallbackTokenGenerator(function() use ($rawUuidGenerator)
{
	return rtrim(base64_encode($rawUuidGenerator->generate()), '=');
});

echo "Random UUID (base64 encoded):\n";
echo $base64UuidGenerator->generate(), "\n\n";

