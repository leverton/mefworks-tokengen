<?php

namespace mef\TokenGenerator;

interface TokenGeneratorInterface
{
    public function generate() : string;
}
