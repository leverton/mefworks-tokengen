<?php

namespace mef\TokenGenerator;

use Traversable;

abstract class AbstractTokenGenerator implements TokenGeneratorInterface, \IteratorAggregate
{
    abstract public function generate(): string;

    public function getIterator(): Traversable
    {
        while (true) {
            yield $this->generate();
        }
    }

    public function iterate(?int $count = null): Traversable
    {
        while ($count === null || $count-- > 0) {
            yield $this->generate();
        }
    }
}
