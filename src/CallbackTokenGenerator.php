<?php

namespace mef\TokenGenerator;

class CallbackTokenGenerator extends AbstractTokenGenerator
{
    /**
     * @var callable
     */
    private $callback;

    /**
     * Constructor
     *
     * @param callable $callback The callback that generates the token.
     */
    public function __construct(callable $callback)
    {
        $this->callback = $callback;
    }

    /**
     * Generate a token by calling the user-specified callback.
     *
     * @return string
     */
    public function generate(): string
    {
        return ($this->callback)();
    }
}
