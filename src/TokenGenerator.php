<?php

namespace mef\TokenGenerator;

class TokenGenerator extends AbstractTokenGenerator
{
    public const DEFAULT_CHARSET = 'BCDFGHJKLMNPQRSTVWXYZbcdfghjkmnpqrstvwxyz23456789';
    public const ALPHA_CHARSET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    public const ALPHANUMERIC_CHARSET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    public const NUMERIC_CHARSET = '0123456789';
    public const LOWER_ALPHA_CHARSET = 'abcdefghijklmnopqrstuvwxyz';
    public const UPPER_ALPHA_CHARSET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    private int $charsetLengthMinusOne;

    /**
     * Constructor
     *
     * @param integer $length  The length of the token
     * @param string  $charset The letters to use for the token
     */
    public function __construct(private int $length = 6, private string $charset = self::DEFAULT_CHARSET)
    {
        if ($this->length <= 0) {
            throw new \InvalidArgumentException('length must be greater than 0');
        }

        if ($this->charset === '') {
            throw new \InvalidArgumentException('charset must not be empty');
        }

        $this->charsetLengthMinusOne = strlen($this->charset) - 1;
    }

    /**
     * @return integer  The length of the generated tokens
     */
    final public function getLength(): int
    {
        return $this->length;
    }

    /**
     * @return string  The character set that the token will be generated from
     */
    final public function getCharset(): string
    {
        return $this->charset;
    }

    /**
     * Generate a random token based on the given character set.
     *
     * @return string   the generated token
     */
    public function generate(): string
    {
        $token = '';
        for ($i = 0; $i < $this->length; ++$i) {
            $token .= $this->charset[mt_rand(0, $this->charsetLengthMinusOne)];
        }

        return $token;
    }
}
