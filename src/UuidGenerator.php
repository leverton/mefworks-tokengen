<?php

namespace mef\TokenGenerator;

class UuidGenerator extends AbstractTokenGenerator
{
    public const RAW_FORMAT = 1;
    public const LOWERCASE_FORMAT = 2;
    public const UPPERCASE_FORMAT = 3;

    private $formatter;

    public function __construct(int $format = self::LOWERCASE_FORMAT)
    {
        switch ($format) {
            case self::LOWERCASE_FORMAT:
                $this->formatter = $this->formatLowerCase(...);
                break;

            case self::UPPERCASE_FORMAT:
                $this->formatter = $this->formatUpperCase(...);
                break;

            case self::RAW_FORMAT:
                $this->formatter = null;
                break;

            default:
                throw new \InvalidArgumentException("format is invalid");
        }
    }

    public function generate(): string
    {
        $rawBytes = random_bytes(16);

        // Set the 6 pre-set bits of v4 Random UUIDs:
        //   4 bits of Byte 6 0100xxxx
        //   2 bits of Byte 8 10xxxxxx
        $rawBytes[6] = chr((ord($rawBytes[6]) & 0x0f) | 0x40);
        $rawBytes[8] = chr((ord($rawBytes[8]) & 0x3f) | 0x80);

        return $this->formatter ? call_user_func($this->formatter, $rawBytes) : $rawBytes;
    }

    private function formatLowerCase(string $bytes): string
    {
        return
            substr(bin2hex($bytes), 0, 8) . '-' .
            substr(bin2hex($bytes), 8, 4) . '-' .
            substr(bin2hex($bytes), 12, 4) . '-' .
            substr(bin2hex($bytes), 16, 4) . '-' .
            substr(bin2hex($bytes), 20, 12);
    }

    private function formatUpperCase(string $bytes): string
    {
        return strtoupper($this->formatLowerCase($bytes));
    }
}
